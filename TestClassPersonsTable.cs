﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security;
using System.Data.SqlClient;
using NUnit.Framework;
using System.Data;


namespace TestSQL
{
    [TestFixture]

    public class TestClassPersonsTable
    {
        SqlConnector connect = new SqlConnector("IPro", "qwerty");

        [Test]
        public void SQLTestPersonTableColumn()
        {
            connect.ConnectToCatalog("DBHW13");
            DataTable result = connect.Execute("SELECT * FROM Persons");


            string resultID = result.Columns[0].ColumnName.ToString();
            string resultFirstName = result.Columns[1].ColumnName.ToString();
            string resultLastName = result.Columns[2].ColumnName.ToString();
            string resultAge = result.Columns[3].ColumnName.ToString();
            string resultCity = result.Columns[4].ColumnName.ToString();
            Assert.AreEqual("PersonID", resultID);
            Assert.AreEqual("FirstName", resultFirstName);
            Assert.AreEqual("LastName", resultLastName);
            Assert.AreEqual("Age", resultAge);
            Assert.AreEqual("City", resultCity);
        }

        [Test]
        public void SQLTestOrdersTableColumn()
        {
            connect.ConnectToCatalog("DBHW13");
            DataTable result = connect.Execute("SELECT * FROM Orders");

            string personID = result.Columns[0].ColumnName.ToString();
            string orderID = result.Columns[1].ColumnName.ToString();
            string orderPrice = result.Columns[2].ColumnName.ToString();

            Assert.AreEqual("personID", personID);
            Assert.AreEqual("orderID", orderID);
            Assert.AreEqual("orderPrice", orderPrice);
        
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(10)]
        [TestCase(19)]
        [TestCase(20)]
        public void SQLTestPersonTable_Unique_ID_Number(int number)
        {
            connect.ConnectToCatalog("DBHW13");
            int result = connect.Execute($"SELECT * FROM Persons WHERE PersonID = {number}").Rows.Count;
            Assert.AreEqual(1, result);
        }

        [Test]
        public void SQLTestPersonsCountInTable()
        {
            connect.ConnectToCatalog("DBHW13");
            int result = connect.Execute($"SELECT * FROM Persons").Rows.Count;
            Assert.AreEqual(20, result);
        }

        [TestCase]
        public void AllCustomerAgeMore18()
        {
            bool flag = false;
            connect.ConnectToCatalog("DBHW13");
            DataTable result = connect.Execute("SELECT*FROM Persons");
            for (int i = 0; i < result.Rows.Count; i++)
            {
                if (int.Parse(result.Rows[i].ItemArray[3].ToString()) < 18)
                {
                    flag = true;
                    break;
                }
                else
                {
                    flag = false;
                }
            }
            Assert.AreEqual(false, flag);
        }

        [Test]
        public void SQLTestPersonsMinAge()
        {
            connect.ConnectToCatalog("DBHW13");
            DataTable persons = connect.Execute($"SELECT * FROM Persons");
            int minFromPersons = int.Parse(persons.Rows[0].ItemArray[3].ToString());
            for (int i = 0; i < persons.Rows.Count; i++)
            {
                if (minFromPersons > int.Parse(persons.Rows[i].ItemArray[3].ToString()))
                {
                    minFromPersons = int.Parse(persons.Rows[i].ItemArray[3].ToString());
                }
            }

            DataTable result = connect.Execute($"SELECT MIN(Age) AS elderAge FROM Persons;");
            Assert.AreEqual(minFromPersons, int.Parse(result.Rows[0].ItemArray[0].ToString()));
        }
        [Test]
        public void SQLTestPersonsMaxAge()
        {
            connect.ConnectToCatalog("DBHW13");
            DataTable persons = connect.Execute($"SELECT * FROM Persons");
            int maxFromPersons = int.Parse(persons.Rows[0].ItemArray[3].ToString());
            for (int i = 0; i < persons.Rows.Count; i++)
            {
                if (maxFromPersons < int.Parse(persons.Rows[i].ItemArray[3].ToString()))
                {
                    maxFromPersons = int.Parse(persons.Rows[i].ItemArray[3].ToString());
                }
            }

            DataTable result = connect.Execute($"SELECT MAX(Age) AS olderAge FROM Persons;");
            Assert.AreEqual(maxFromPersons, int.Parse(result.Rows[0].ItemArray[0].ToString()));
        }


        [Test]
        public void CheckThatWeHavePersonsInDataBase()
        {
            connect.ConnectToCatalog("DBHW13");
            DataTable result = connect.Execute("SELECT * FROM Persons");
            bool flag = false;

            if (result.Rows.Count > 0)
            {
                flag = true;

            }
            else
            {
                flag = false;
            }

            Assert.AreEqual(true, flag);

        }

        #region Add and Delete Person from PersonsTable

        //[Test]
        //public void CheckCreateNewPersonsIntoTable()
        //{

        //    connect.ConnectToCatalog("DBHW13");
        //    DataTable startTable = connect.Execute("SELECT*FROM Persons");
        //    connect.AddCommand($"INSERT INTO Persons (PersonID, FirstName, LastName, Age, City) VALUES({startTable.Rows.Count + 1}, 'FirstName', 'LastName', 100, 'CityTest');");
        //    DataTable result = connect.Execute("SELECT*FROM Persons");
        //    Assert.AreEqual((startTable.Rows.Count + 1), result.Rows.Count);

        //}

        //[Test]
        //public void CheckDeleteNewPersonsIntoTable()
        //{

        //    connect.ConnectToCatalog("DBHW13");
        //    DataTable startTable = connect.Execute("SELECT*FROM Persons");
        //    connect.AddCommand($"DELETE FROM Persons WHERE LastName = 'LastName'");
        //    DataTable result = connect.Execute("SELECT*FROM Persons");
        //    Assert.AreEqual((startTable.Rows.Count - 1), result.Rows.Count);

        //}

        #endregion
    }
}

