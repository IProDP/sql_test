﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security;
using System.Data.SqlClient;
using NUnit.Framework;
using System.Data;


namespace TestSQL
{
    [TestFixture]

    public class TestClassOrdersTable
    {
        SqlConnector connect = new SqlConnector("IPro", "qwerty");


        [Test]
        public void SQLTestOrdersTableColumn()
        {
            connect.ConnectToCatalog("DBHW13");
            DataTable result = connect.Execute("SELECT * FROM Orders");

            string personID = result.Columns[0].ColumnName.ToString();
            string orderID = result.Columns[1].ColumnName.ToString();
            string orderPrice = result.Columns[2].ColumnName.ToString();

            Assert.AreEqual("personID", personID);
            Assert.AreEqual("orderID", orderID);
            Assert.AreEqual("orderPrice", orderPrice);

        }
 
        [TestCase]
        public void CheckOrderWithOrdersSumNull()
        {
            bool flag = false;
            connect.ConnectToCatalog("DBHW13");
            DataTable result = connect.Execute("SELECT * FROM Orders");
            for (int i = 0; i < result.Rows.Count; i++)
            {
                if (result.Rows[i].ItemArray[2].ToString().Length == 0)
                {
                    flag = true;
                    break;
                }
                else
                {
                    flag = false;
                }
            }
            Assert.AreEqual(false, flag);
        }



        [Test]
        public void CheckThatWeHaveOrdersInDataBase()
        {
            connect.ConnectToCatalog("DBHW13");
            DataTable result = connect.Execute("SELECT * FROM Orders");
            bool flag = false;

            if (result.Rows.Count > 0)
            {
                flag = true;

            }
            else
            {
                flag = false;
            }

            Assert.AreEqual(true, flag);

        }

        [Test]
        public void CheckThatOrdersWasForPersonsWithRegistration()
        {
            bool flag = false;
            int count = 0;
            connect.ConnectToCatalog("DBHW13");
            DataTable resultOrders = connect.Execute("SELECT * FROM Orders");
            DataTable resultPersons = connect.Execute("SELECT * FROM Persons");
            for (int i = 0; i < resultPersons.Rows.Count; i++)
            {
                for (int j = 0; j < resultOrders.Rows.Count; j++)
                {
                    if (resultOrders.Rows[j].ItemArray[0].ToString() == resultPersons.Rows[i].ItemArray[0].ToString())
                    {
                        count++;

                    }
                    else
                    {
                        continue;
                    }
                }

            }
            if (count == resultOrders.Rows.Count)
            {
                flag = true;
            }
            Assert.AreEqual(true, flag);
        }

        [Test]
        public void SQLTestOrdersMinSum()
        {
            connect.ConnectToCatalog("DBHW13");
            DataTable orders = connect.Execute($"SELECT * FROM Orders");
            decimal minFromOrders = decimal.Parse(orders.Rows[0].ItemArray[2].ToString());
            for (int i = 0; i < orders.Rows.Count; i++)
            {
                if (minFromOrders > decimal.Parse(orders.Rows[i].ItemArray[2].ToString()))
                {
                    minFromOrders = decimal.Parse(orders.Rows[i].ItemArray[2].ToString());
                }
            }

            DataTable result = connect.Execute($"SELECT MIN(orderPrice) AS minOrder FROM Orders;");
            Assert.AreEqual(minFromOrders, decimal.Parse(result.Rows[0].ItemArray[0].ToString()));
        }

        [Test]
        public void SQLTestOrdersMaxSum()
        {
            connect.ConnectToCatalog("DBHW13");
            DataTable orders = connect.Execute($"SELECT * FROM Orders");
            decimal maxFromOrders = decimal.Parse(orders.Rows[0].ItemArray[2].ToString());
            for (int i = 0; i < orders.Rows.Count; i++)
            {
                if (maxFromOrders < decimal.Parse(orders.Rows[i].ItemArray[2].ToString()))
                {
                    maxFromOrders = decimal.Parse(orders.Rows[i].ItemArray[2].ToString());
                }
            }

            DataTable result = connect.Execute($"SELECT MAX(orderPrice) AS maxOrder FROM Orders;");
            Assert.AreEqual(maxFromOrders, decimal.Parse(result.Rows[0].ItemArray[0].ToString()));
        }

        [Test]
        public void SQLTestOrdersAVGSum()
        {
            connect.ConnectToCatalog("DBHW13");
            DataTable orders = connect.Execute($"SELECT * FROM Orders");
            decimal AVGFromOrdersSum = 0;
            
            for (int i = 0; i < orders.Rows.Count; i++)
            {
                AVGFromOrdersSum = AVGFromOrdersSum + decimal.Parse(orders.Rows[i].ItemArray[2].ToString());
            }
            decimal AVGFromOrders = AVGFromOrdersSum/orders.Rows.Count;
            DataTable result = connect.Execute($"SELECT AVG(Orders.orderPrice) As AVG_ordersPrice FROM Orders");
            Assert.AreEqual(AVGFromOrders, decimal.Parse(result.Rows[0].ItemArray[0].ToString()));
        }


        #region Add and Delete order from table
        //[Test]
        //public void CheckCreateNewOrdersIntoTable()
        //{

        //    connect.ConnectToCatalog("DBHW13");
        //    DataTable personStart = connect.Execute("SELECT*FROM Persons");
        //    int personID = new Random().Next(1, personStart.Rows.Count);
        //    int orderPriceRandom = new Random().Next(1, 1000);
        //    DataTable orderStart = connect.Execute("SELECT*FROM Orders");
        //    connect.AddCommand($"INSERT INTO Orders (personID, orderID, orderPrice) VALUES({personID}, {orderStart.Rows.Count+1}, {orderPriceRandom});");
        //    DataTable result = connect.Execute("SELECT*FROM Orders");
        //    Assert.AreEqual((orderStart.Rows.Count + 1), result.Rows.Count);
        //    Assert.AreEqual(orderPriceRandom, result.Rows[result.Rows.Count-1].ItemArray[2]);

        //}

        //[Test]
        //public void CheckDeleteNewOrdersIntoTable()
        //{

        //    connect.ConnectToCatalog("DBHW13");
        //    DataTable startTable = connect.Execute("SELECT*FROM Orders");
        //    connect.AddCommand($"DELETE FROM Orders WHERE orderID = {startTable.Rows.Count}");
        //    DataTable result = connect.Execute("SELECT*FROM Orders");
        //    Assert.AreEqual((startTable.Rows.Count-1), result.Rows.Count);

        //}
        #endregion
    }
}

